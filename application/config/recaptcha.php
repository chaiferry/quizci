<?php
defined('BASEPATH') or exit('No direct script access allowed');

// To use reCAPTCHA, you need to sign up for an API key pair for your site.
// link: http://www.google.com/recaptcha/admin
$config['recaptcha_site_key'] = '6LciO6oUAAAAAHe2w4zho_OWR4Pb1z3OhsjTBvNU';
$config['recaptcha_secret_key'] = '6LciO6oUAAAAALRCrxEpt-7rUirqwdMwK7TJJ4hC';

// reCAPTCHA supported 40+ languages listed here:
// https://developers.google.com/recaptcha/docs/language
$config['recaptcha_lang'] = 'en';

/* End of file recaptcha.php */
/* Location: ./application/config/recaptcha.php */
