<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Tabel extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Login_Model');
		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->load->library('user_agent');
		$this->load->library('session');
	}

	public function index()
	{
		//Ambil IP Address
		if ($this->agent->is_browser()) {
			$agent['media'] = $this->agent->browser() . ' ' . $this->agent->version();
		} elseif ($this->agent->is_mobile()) {
			$agent['media'] = $this->agent->mobile();
		} else {
			$agent['media'] = 'Data user gagal di dapatkan';
		}

		$agent['os'] = $this->agent->platform();
		$agent['ip'] = $this->input->ip_address();

		$data['judul'] = 'Welcome';
		$this->load->view('templates/header', $data);
		$this->load->view('tabel/index', $agent);
		$this->load->view('templates/footer');
	}

	//Load halaman table
	public function data()
	{
		$data['judul'] = 'Tabel Login';
		// $data['kelamin'] = ['--','Laki-laki','Perempuan'];
		$data['login'] = $this->Login_Model->getAllLogin();
		if ($this->input->post('searching')) {
			$data['login'] = $this->Login_Model->caridata();
		}
		$this->load->view('templates/header', $data);
		$this->load->view('tabel/tabel', $data);
		$this->load->view('templates/footer');
	}

	public function hapus($id)
	{	
		//Klo yang di apus user yang lagi login
		if($this->session->userdata('id') == $id)
		{
			$this->Login_Model->hapusDataLogin($id);
			$this->session->set_flashdata('flash', 'dihapus');
			redirect('home/logout');	
		}

		//buat user biasa
		$this->Login_Model->hapusDataLogin($id);
		$this->session->set_flashdata('flash', 'dihapus');
		redirect('tabel/data');	
	}


	public function ubah()
	{
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('tanggallahir', 'Tanggal Lahir', 'trim|required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('jk', 'Jenis Kelamin', 'trim|required');
		$this->form_validation->set_rules('nomor', 'Nomor Telepon', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$id = $this->input->post('user_id');
		$user = $this->db->get_where('tabelemail', ['id' => $id])->row_array();

		//Buat Cek sama ga ama user lain yang daftar di database
		$dataditable = $this->Login_Model->getAllLogin();
		$batasaja = count($dataditable);
		$cek_nama = $this->input->post('nama');
		$cek_email = $this->input->post('email');
		for ($i = 0; $i < $batasaja; $i++) {
			if ($cek_nama == $dataditable[$i]['Nama']) 
			{
				if ($cek_nama != $user['Nama']) {
					$sama = true;
					break;
				}
			}
			if ($cek_email == $dataditable[$i]['Email']) 
			{
				if ($cek_email != $user['Email']) {
					$sama = true;
					break;
				}
			}
		}

		if ($this->form_validation->run() == false || $sama == true)
		{
			$data['judul'] = 'Tabel Login';
			$data['login'] = $this->Login_Model->getAllLogin();
			$sama = false;
			$this->session->set_flashdata('gagal', 'lengkap');
			$this->load->view('templates/header', $data);
			$this->load->view('tabel/tabel', $data);
			$this->load->view('templates/footer');
		} 
		else
		{	
			//Lolos validasi
			//Klo usernya ada ubah nama ato email, hrus buat baru qrcode nya
			if ($this->input->post('nama') != $user['Nama'] || $this->input->post('email') != $user['Email']) 
			{
				$this->load->library('ciqrcode'); //pemanggilan library QR CODE

				$config['cacheable']    = true; //boolean, the default is true
				$config['cachedir']     = './assets/'; //string, the default is application/cache/
				$config['errorlog']     = './assets/'; //string, the default is application/logs/
				$config['imagedir']     = './assets/images/'; //direktori penyimpanan qr code
				$config['quality']      = true; //boolean, the default is true
				$config['size']         = '1024'; //interger, the default is 1024
				$config['black']        = array(224, 255, 255); // array, default is array(255,255,255)
				$config['white']        = array(70, 130, 180); // array, default is array(0,0,0)
				$this->ciqrcode->initialize($config);

				$image_name = $this->input->post('nama') . '.png'; //buat name dari qr code sesuai dengan nim

				$params['data'] = $this->input->post('nama') . ' & ' . $this->input->post('email'); //data yang akan di jadikan QR CODE
				$params['level'] = 'H'; //H=High
				$params['size'] = 10;
				$params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
				$this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
			
			} 
			else 
			{
				$image_name = $user['qr_code'];
			
			}

			if($this->session->userdata('Nama') == $user['Nama'] && $this->session->userdata('Email')==$user['Email'] && $this->session->userdata('image') == $user['image'])
			{
				$this->session->sess_destroy();
				$this->Login_Model->ubahDataLogin($image_name);	
				redirect('home/gantisession/'.$id);
			}
			else
			{
				//validasinya sukses
				$this->Login_Model->ubahDataLogin($image_name);
				$this->session->set_flashdata('ganti', 'diubah');
				redirect('tabel/data');
			}
		}	
	}


	//Buat load datatables, ini isi tablenya
	public function get_data_user()
	{
		$list = $this->Login_Model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] =  '<img style="width: 100px;" src="../assets/profil/' . $field->image . '"/>';
			$row[] = $field->Nama;
			$row[] = $field->Email;
			$row[] = '<img style="width: 100px;" src="../assets/images/' . $field->qr_code . '"/>';
			$row[] =
				'<button type="button" class="btn btn-info mx-1 profil" data-toggle="modal" data-target="#modalprofil" id="' . $field->id . '">Profile</button>
	            <button type="button" class="btn btn-success mx-1 edit" id="' . $field->id . '" data-toggle="modal" data-target="#modaledit" >Edit</button>
	            <a href="' . base_url() . 'Tabel/hapus/' . $field->id . '" class="btn btn-danger mx-1">Delete</a>';

			$data[] = $row;
		}

		$output = array(
			"draw" => intval($_POST['draw']),
			"recordsTotal" => $this->Login_Model->count_all(),
			"recordsFiltered" => $this->Login_Model->count_filtered(),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}

	public function edit_user()
	{
		$output = array();
		$data = $this->Login_Model->data_single_user($_POST['user_id']);
		foreach ($data as $row) {
			$output['Nama'] = $row->Nama;
			$output['Email'] = $row->Email;
			$output['TanggalLahir'] = $row->TanggalLahir;
			$output['Alamat'] = $row->Alamat;
			$output['NomorTelepon'] = $row->NomorTelepon;
			$output['JenisKelamin'] = $row->JenisKelamin;
			$output['image'] = $row->image;

			echo json_encode($output);
		}
	}

	public function profil_user()
	{
		$output = array();
		$data = $this->Login_Model->data_single_user($_POST['user_id']);
		foreach ($data as $row) {
			$output['Nama'] = $row->Nama;
			$output['Email'] = $row->Email;
			$output['TanggalLahir'] = $row->TanggalLahir;
			$output['Alamat'] = $row->Alamat;
			$output['NomorTelepon'] = $row->NomorTelepon;
			$output['JenisKelamin'] = $row->JenisKelamin;
			
			echo json_encode($output);
		}
	}

}
