<?php

class Home extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Login_Model');
		$this->load->helper('string');
		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->library('phpmailer_lib');
	}

	public function index($nama = '')
	{
		$this->form_validation->set_rules('emailuser', ' Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('passworduser', 'Password', 'trim|required');

		$data = array(
			'captcha' => $this->recaptcha->getwidget(), //menampilkan recaptcha
			'script_captcha' => $this->recaptcha->getScriptTag(), //Javascript recaptcha nya buat di polos.php
		);

		if ($this->form_validation->run() == false) {
			$data['judul'] = 'Halaman utama';
			$data['nama'] = $nama;
			$this->load->view('templates/polos', $data);
			$this->load->view('login/index', $data);
			$this->load->view('templates/polos2');
		} else {
			//validasinya sukses
			$this->_login();
		}
	}

	private function _login()
	{
		$email = $this->input->post('emailuser');
		$password = $this->input->post('passworduser');
		$user = $this->db->get_where('tabelemail', ['Email' => $email])->row_array();
		$recaptcha = $this->input->post('g-recaptcha-response');
		$respon = $this->recaptcha->verifyResponse($recaptcha);

		//Cek usernya ada didatabase apa kaga
		if ($user) {
			// cek password
			if (password_verify($password, $user['Password'])) {
				if (!isset($respon['success']) || $respon['success'] <> true) {
					$this->session->set_flashdata('captcha', Recaptcha);
					redirect('home');
				} else {
					//Udh verifikasi atau belom
					if ($user['status'] != "ok") {
						$this->session->set_flashdata('noverif', diverifikasi);
						redirect('home');
					} else {
						//Klo udh ok semua, msuk deh
						$data_session = array(
							"id" => $user['id'],
							"Nama" => $user['Nama'],
							"Email" => $user['Email'],
							"image" => $user['image'],
							"qrcode" => $user['qr_code'],
						);
						$this->session->set_userdata($data_session);
						redirect('tabel/index');
					}
				}
			} else {
				$this->session->set_flashdata('pesan', Password);
				redirect('home');
			}
		} else {
			$this->session->set_flashdata('message', Registered);
			redirect('home');
		}
	}

	public function signup()
	{
		$this->form_validation->set_rules('nama', 'Name', 'required|trim|is_unique[tabelemail.Nama]');
		$this->form_validation->set_rules('tanggallahir', 'Tanggal Lahir', 'trim|required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('jk', 'Jenis Kelamin', 'trim|required');
		$this->form_validation->set_rules('nomor', 'Nomor Telepon', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[tabelemail.Email]');
		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[3]|matches[pass]');
		$this->form_validation->set_rules('pass', 'Password', 'required|trim|min_length[3]|matches[password]');

		if ($this->form_validation->run() == false) {
			$this->session->set_flashdata('kosong', 'lengkap');
			redirect(base_url());
		} else {
			//Buat PHP MAILER kirim verifikasi akun
			$nama = $this->input->post('nama');
			$email = $this->input->post('email');
			$code = random_string('md5');
			$key = base64_encode($email . '' . $code);
			// PHPMailer object
			$mail = $this->phpmailer_lib->load();

			// SMTP configuration
			$mail->isSMTP();
			$mail->Host     = 'smtp.gmail.com';
			$mail->SMTPAuth = true;
			$mail->Username = 'deavengers987@gmail.com';
			$mail->Password = 'theavengers987';
			$mail->SMTPSecure = 'tls';
			$mail->Port     = 587;

			$mail->setFrom('deavengers987@gmail.com', 'S.H.I.E.L.D');
			$mail->addReplyTo('deavengers987@gmail.com', 'S.H.I.E.L.D');

			// Add a recipient
			$mail->addAddress($email);

			// Email subject
			$mail->Subject = 'Verify Your Account';

			// Set email format to HTML
			$mail->isHTML(true);

			// Email body content
			$mailContent = "
		        	<h1>Hi, " . $nama . " </h1>
		        	<h2>Click <a href=" . base_url() . 'home/verif/' . $key . ">here</a> to Verify your account</h2>
		        	";
			$mail->Body = $mailContent;
			// Send email
			if (!$mail->send()) {
				echo 'Message could not be sent.';
				echo 'Mailer Error: ' . $mail->ErrorInfo;
			} else {
				//Klo send verifikasi berhasi, masukin data nya ke database

				//Buat bikin QRCODE nya
				$id = $this->input->post('nama');
				$data = $id . ' & ' . $this->input->post('email');
				$this->load->library('ciqrcode'); //pemanggilan library QR CODE

				$config['cacheable']    = true; //boolean, the default is true
				$config['cachedir']     = './assets/'; //string, the default is application/cache/
				$config['errorlog']     = './assets/'; //string, the default is application/logs/
				$config['imagedir']     = './assets/images/'; //direktori penyimpanan qr code
				$config['quality']      = true; //boolean, the default is true
				$config['size']         = '1024'; //interger, the default is 1024
				$config['black']        = array(224, 255, 255); // array, default is array(255,255,255)
				$config['white']        = array(70, 130, 180); // array, default is array(0,0,0)
				$this->ciqrcode->initialize($config);

				$image_name = $id . '.png'; //buat name dari qr code sesuai dengan nama user

				$params['data'] = $data; //data yang akan di jadikan QR CODE
				$params['level'] = 'H'; //H=High
				$params['size'] = 10;
				$params['savename'] = FCPATH . $config['imagedir'] . $image_name; //simpan image QR CODE ke folder assets/images/
				$this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

				$this->Login_Model->SignUp($image_name, $key);
				$this->session->set_flashdata('reset', sent);
				redirect(base_url());
			}
		}
	}

	public function verif($key)
	{
		$user = $this->db->get_where('tabelemail', ['status' => $key])->row_array();
		$id = $user['id'];
		$this->Login_Model->verif($id);
		$this->session->set_flashdata('verif', 'diverifikasi');
		redirect(base_url());
	}


	public function send()
	{
		$email = $this->input->post('emailuser');
		$data = $this->Login_Model->getAllLogin();
		$batas = count($data);

		//Proses cek email yang di input ke database
		for ($i = 0; $i < $batas; $i++) {
			if ($email == $data[$i]['Email']) {
				$checked = true; //Ada emailnya
				$no = $i;
				break;
			}
		}

		if ($checked) {

			// PHPMailer object
			$mail = $this->phpmailer_lib->load();

			// SMTP configuration
			$mail->isSMTP();
			$mail->Host     = 'smtp.gmail.com';
			$mail->SMTPAuth = true;
			$mail->Username = 'deavengers987@gmail.com';
			$mail->Password = 'theavengers987';
			$mail->SMTPSecure = 'tls';
			$mail->Port     = 587;

			$mail->setFrom('deavengers987@gmail.com', 'S.H.I.E.L.D');
			$mail->addReplyTo('deavengers987@gmail.com', 'S.H.I.E.L.D');

			// Add a recipient
			$mail->addAddress($email);

			// Email subject
			$mail->Subject = 'Reset Password';

			// Set email format to HTML
			$mail->isHTML(true);

			// Email body content
			$mailContent = "
	        	<h2>Click <a href=" . base_url() . 'Home/change_pw/' . $no . ">here</a> to reset your password</h2>
	        	";
			$mail->Body = $mailContent;

			// Send email
			if (!$mail->send()) {
				echo 'Message could not be sent.';
				echo 'Mailer Error: ' . $mail->ErrorInfo;
			} else {
				$this->session->set_flashdata('reset', sent);
				redirect('home');
			}
		} else {
			$this->session->set_flashdata('message', Registered);
			redirect('home');
		}
	}

	//Load ke halaman change pw
	public function change_pw($id)
	{
		$data['judul'] = 'Change Password';
		$data['login'] = $this->Login_Model->getAllLogin();
		$data['no'] = $id;
		$this->load->view('templates/polos3', $data);
		$this->load->view('login/chgpw', $data);
		$this->load->view('templates/polos4');
	}

	//Proses ganti password
	public function ubah()
	{
		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[3]|matches[pass]');
		$this->form_validation->set_rules('pass', 'Password', 'required|trim|min_length[3]|matches[password]');

		if ($this->form_validation->run() == false) {

			$this->session->set_flashdata('gagal', 'lengkap');
			$this->index();
		} else {
			//validasinya sukses
			$this->Login_Model->ubahPassword();
			$this->session->set_flashdata('gantipw', 'diubah');
			redirect(base_url());
		}
	}

	//Proses ganti session klo di table kita edit user
	public function gantisession($id)
	{
		$ganti = $this->db->get_where('tabelemail', ['id' => $id])->row_array();
		$data_session = array(
			"id" => $ganti['id'],
			"Nama" => $ganti['Nama'],
			"Email" => $ganti['Email'],
			"image" => $ganti['image'],
			"qrcode" => $ganti['qr_code'],
		);
		$this->session->set_userdata($data_session);
		$this->session->set_flashdata('ganti', 'diubah');
		redirect('tabel/data');
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}
}
