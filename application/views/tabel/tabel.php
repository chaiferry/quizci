<div id="isi" class="text-center container-fluid" style="margin-bottom:200px;">

	<?php if ($this->session->flashdata('flash')) : ?>
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-success alert-dismissible fade show" role="alert">
					Data User <strong>berhasil</strong> <?php echo $this->session->flashdata('flash'); ?>.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<?php if ($this->session->flashdata('ganti')) : ?>
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-success alert-dismissible fade show" role="alert">
					Data User <strong>berhasil</strong> <?php echo $this->session->flashdata('ganti'); ?>.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<?php if ($this->session->flashdata('gagal')) : ?>
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					Maaf, Data User yang di ubah <strong>harus</strong> <?php echo $this->session->flashdata('gagal'); ?> atau Nama atau email yang anda input sudah terdaftar.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</div>
		</div>
	<?php endif; ?>

		<table id="table" class="table table-bordered display table-responsive-md" cellspacing="0" width="100%">
			<thead class="purple-gradient">
				<tr>
					<th>No</th>
					<th>Profile</th>
					<th>Nama</th>
					<th>Email</th>
					<th>QR Code</th>
					<th>Option</th>
				</tr>
			</thead>
			<div>

				<tbody>
				</tbody>
		</table>

		<div class="modal fade bd-example-modal-lg" id="modalprofil" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header purple-gradient">
						<h3 id="exampleModalLabel"><span id="NamaProfil"></span></h3>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body text-left col-md-12 row">
						<div class="col-md-8">
							<div>
								<label>Tanggal Lahir :</label>
								<span id="TanggalLahirProfil"></span>
							</div>

							<div>
								<label>Jenis Kelamin :</label>
								<span id="JenisKelaminProfil"></span>
							</div>

							<div>
								<label>Alamat :</label>
								<span id="AlamatProfil"></span>
							</div>

							<div>
								<label>Email :</label>
								<span id="EmailProfil"></span>
							</div>

							<div>
								<label>Nomor Telepon :</label>
								<span id="NomorTeleponProfil"></span>
							</div>

						</div>

						<div class="col-md-4 p-0 float-right">
							<img src="../gambar/avengers.png" width="100" height="100" class="float-right">
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade bd-example-modal-lg" id="modaledit" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header purple-gradient">
						<h3 class="modal-title" id="exampleModalLabel">Change Data</h3>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body col-md-12 text-left">

						<form action="<?= base_url() ?>tabel/ubah" method="post" class="form-group" enctype='multipart/form-data'>
							<input class="form-control" type="hidden" name="user_id" id="user_id">
							<div class="md-form mb-3">
								<i class="fas fa-users prefix"></i>
								<input id="Nama" class="form-control mb-1" type="text" name="nama" placeholder="New Name" required>
								<label data-error="wrong" data-success="right">Name:</label>
								<div class="invalid-feedback">Please select a valid date.</div>
							</div>

							<div class="md-form mb-1">
								<i class="fas fa-baby prefix"></i>
								<input id="TanggalLahir" class="form-control" type="text" name="tanggallahir" placeholder="Born Day" required>
								<label>Born Day:</label>
								<div class="invalid-feedback">Please select a valid date.</div>
							</div>

							<div class="md-form mb-3">
								<i class="fas fa-map-marker-alt prefix"></i>
								<input type="text" id="Alamat" class="form-control" rows=" 3" name="alamat" placeholder="address" required>
								<label>Address:</label>
								<div class="invalid-feedback">Please select a valid date.</div>
							</div>

							<div class="mb-2">
								<i class="fas fa-venus-mars prefix"></i>
								<div class="form-check form-check-inline">
									<input type="radio" name="jk" id="laki" class="form-check-input" value="1" required>
									<label class="form-check-label" for="laki">Laki-Laki</label>
									<div class="invalid-feedback">Please select a valid date.</div>
								</div>
								<div class="form-check form-check-inline">
									<input type="radio" name="jk" id="gadis" class="form-check-input" value="2" required>
									<label class="form-check-label" for="gadis">Perempuan</label>
									<div class="invalid-feedback">Please select a valid date.</div>
								</div>
							</div>

							<div class="md-form mb-3">
								<i class="fas fa-phone prefix"></i>
								<input id="NomorTelepon" class="form-control mb-1" type="number" name="nomor" placeholder="Number Phone" required>
								<label>Phone Number:</label>
								<div class="invalid-feedback">Please select a valid date.</div>
							</div>

							<div class="md-form mb-3">
								<i class="fas fa-envelope prefix"></i>
								<input id="Email" class="form-control mb-1" type="email" name="email" placeholder="New Email" required>
								<label>Email:</label>
								<div class="invalid-feedback">Please select a valid date.</div>
							</div>
							<div class="md-form mb-5">
								<div class="file-field">
									<div class="btn purple-gradient btn-sm float-left">
										<span><i class="fas fa-cloud-upload-alt mr-2" aria-hidden="true"></i>Choose files</span>
										<input type="file" name="photo" id="photo" class="custom-file-input" required>
										<div class="invalid-feedback">Please input your image.</div>
									</div>
									<div class="file-path-wrapper">
										<input id="isifoto" class="file-path validate" type="text" placeholder="Upload your photo profile">
									</div>
								</div>
							</div>
							<div class="float-right">
								<button type="submit" id="action" name="action" class="btn purple-gradient btn-rounded">Save changes</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

</div>