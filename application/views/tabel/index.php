    <!-- News jumbotron -->
    <div class="text-center hoverable p-4" style="background-color: white;">

      <!-- Grid row -->
      <div class="row">

        <!-- Grid column -->
        <div class="col-md-4 offset-md-1 mx-3 my-3">

          <!-- Featured image -->
          <div class="view overlay">
            <img src="../assets/profil/<?= $this->session->userdata('image'); ?>" class="img-fluid img-responsive" style="max-height:300px; margin-left:50px" alt="Sample image for first version of blog listing">
            <a>
              <div class="mask"></div>
            </a>
          </div>

        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-7 text-md-left ml-3 mt-3">

          <!-- Excerpt -->

          <h4 class="h3 mb-4">QUOTES</h4>
          <h5>
            <p class="text-monospace">Cinta tidak mengenal pemaksaan. Ia selalu menjadi misteri. Datang dan pergi begitu saja meninggalkan kenangan yang begitu sulit untuk dilupakan.</p>
          </h5>
          <h5>
            <p class="text-monospace">Cinta itu bukanlah berusaha menemukan sosok pasangan sempurna, namun belajar melihat ketidaksempurnaan pasangan kita dengan sempura.</p>
          </h5>
          <p class="font-italic">by <a><strong>HAF</strong></a>, 23/06/2019</p>
          <div>
            <button type="button" class="btn btn-rounded btn-success" data-toggle="modal" data-target="#sideModalBR">IP ADDRESS</button>
            <button type="button" class="btn btn-rounded btn-success" data-toggle="modal" data-target="#midModalBR">QR CODE</button>
          </div>

        </div>
      </div>


      <div class="modal fade right" id="sideModalBR" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <!-- Add class .modal-side and then add class .modal-top-right (or other classes from list above) to set a position to the modal -->
        <div class="modal-dialog modal-side modal-bottom-right" role="document">


          <div class="modal-content">
            <div class="modal-header purple-gradient">
              <h4 class="modal-title" id="myModalLabel">Di Akses dari :</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="container">
                <table class="table table-success table-bordered">
                  <tbody>
                    <tr>
                      <th scope="row">Browser :</th>
                      <th><?= $media ?></th>
                    </tr>
                    <tr>
                      <th scope="row">Sistem Operasi :</th>
                      <th><?= $os ?></th>
                    </tr>
                    <tr>
                      <th scope="row">IP Address :</th>
                      <th><?= $ip ?></th>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade" id="midModalBR" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">

        <!-- Add .modal-dialog-centered to .modal-dialog to vertically center the modal -->
        <div class="modal-dialog modal-dialog-centered" role="document">


          <div class="modal-content">
            <div class="modal-header purple-gradient">
              <h5 class="modal-title" id="ModalLabel" style="margin-left:160px">QR CODE Anda :</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <img src="../assets/images/<?= $this->session->userdata('qrcode') ?>" style="width: 230px; height: 230px;">
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>


    </div>