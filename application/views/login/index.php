<div>

	<?php if ($this->session->flashdata('flash')) : ?>
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="alert alert-success alert-dismissible fade show" role="alert">
					Data User <strong>berhasil</strong> <?php echo $this->session->flashdata('flash'); ?>.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<?php if ($this->session->flashdata('message')) : ?>
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					Email is not<strong></strong> <?php echo $this->session->flashdata('message'); ?>.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<?php if ($this->session->flashdata('captcha')) : ?>
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					Dont forget to <strong><?php echo $this->session->flashdata('captcha'); ?></strong>.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<?php if ($this->session->flashdata('pesan')) : ?>
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					Wrong<strong> <?php echo $this->session->flashdata('pesan'); ?>. </strong>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<?php if ($this->session->flashdata('gantipw')) : ?>
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-success alert-dismissible fade show" role="alert">
					Password <strong>berhasil</strong> <?php echo $this->session->flashdata('gantipw'); ?>.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</div>
		</div>
	<?php endif; ?>


	<?php if ($this->session->flashdata('kosong')) : ?>
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					Maaf data user yang anda isi <strong>harus</strong> <?php echo $this->session->flashdata('kosong'); ?> atau Email atau nama yang anda input<strong>sudah terdaftar</strong>.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<?php if ($this->session->flashdata('noverif')) : ?>
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					Maaf data user yang anda isi <strong>belum <?php echo $this->session->flashdata('noverif'); ?></strong>.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<?php if ($this->session->flashdata('verif')) : ?>
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="alert alert-success alert-dismissible fade show" role="alert">
					Selamat, akun anda <strong>berhasil</strong> <?php echo $this->session->flashdata('verif'); ?>.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<?php if ($this->session->flashdata('reset')) : ?>
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="alert alert-success alert-dismissible fade show" role="alert">
					Message has been <strong> <?php echo $this->session->flashdata('reset'); ?></strong>.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</div>
		</div>
	<?php endif; ?>


	<form class="container form-gradient col-md-5 mt-5" method="post" action="<?= base_url(); ?>home">

		<!--Form with header-->
		<div class="card">

			<!--Header-->
			<div class="header pt-3 purple-gradient">

				<div class="container">
					<h3 class="deep-grey-text font-weight-bold text-center mt-3 mb-4 pb-1 mx-5">LOGIN</h3>
				</div>

			</div>
			<!--Header-->

			<div class="card-body mx-2 mt-2">

				<!--Body-->

				<div class="md-form">
					<i class="fas fa-user-astronaut prefix"></i>
					<input type="email" class="form-control" name="emailuser" value="<?= set_value('emailuser') ?>">
					<label for="Form-email4">Email</label>
					<?= form_error('emailuser', '<small class="text-danger pl-3">', '</small>'); ?>
				</div>

				<div class="md-form pb-3">
					<i class="fas fa-shield-alt prefix"></i>
					<input type="password" id="Form-pass4" name="passworduser" class="form-control">
					<label for="Form-pass4">Password</label>
					<div class=" container col-md-9 mt-4">
						<?php echo $captcha ?>
					</div>
				</div>

				<!--Grid row-->
				<div class="text-center mb-4">
					<button type="submit" class="btn purple-gradient btn-block btn-rounded">Login</button>
				</div>
				<p class="font-small grey-text d-flex justify-content-center">Forgot <a data-toggle="modal" class="purple-text ml-1 font-weight-bold" data-target="#chgpw"> Password?</a></p>
				<p class="font-small grey-text d-flex justify-content-center">Don't have an account? <a class="purple-text ml-1 font-weight-bold" data-toggle="modal" data-target="#signup"> Sign up</a></p>
			</div>
		</div>
	</form>


	<div class="modal fade" id="signup" tabindex="-1" role="dialog" aria-labelledby="signupLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header text-center purple-gradient">
					<h3 class="modal-title w-100 font-weight-bold" id="signupLabel">Sign up</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form class="needs-validation container" action="<?= base_url() ?>home/signup" method="post" novalidate enctype='multipart/form-data'>
						<div class="md-form mb-3">
							<i class="fas fa-user-tie prefix"></i>
							<input type="text" name="nama" id="orangeForm-name" class="form-control" required>
							<label data-error="wrong" data-success="right" for="orangeForm-name">Name</label>
							<div class="invalid-feedback">Please insert your Name.</div>
						</div>
						<div class="md-form mb-3">
							<i class="fas fa-baby prefix"></i>
							<input type="text" name="tanggallahir" id="orangeForm-born" class="form-control datepicker" required>
							<label data-error="wrong" data-success="right" for="orangeForm-born">Born Day</label>
							<div class="invalid-feedback">Please select a valid date.</div>
						</div>
						<div class="md-form mb-3">
							<i class="fas fa-map-marker-alt prefix"></i>
							<input type="text" name="alamat" rows="3" id="orangeForm-add" class="form-control" required>
							<label data-error="wrong" data-success="right" for="orangeForm-add">Address</label>
							<div class="invalid-feedback">Please ynsert your Address.</div>
						</div>
						<div class="md-form mb-3">
							<i class="fas fa-venus-mars prefix"></i>
							<div class="form-check form-check-inline ml-5">
								<input type="radio" name="jk" id="orangeForm-jk1" class="form-check-input" value="1" required>
								<label class="form-check-label" for="orangeForm-jk1">Laki-Laki</label>
								<div class="invalid-feedback">Please choose one.</div>
							</div>
							<div class="form-check form-check-inline">
								<input type="radio" name="jk" id="orangeForm-jk2" class="form-check-input" value="2" required>
								<label class="form-check-label" for="orangeForm-jk2">Perempuan</label>
							</div>
						</div>
						<div class="md-form mb-3">
							<i class="fas fa-phone prefix"></i>
							<input type="number" name="nomor" id="orangeForm-nmr" class="form-control" required>
							<label data-error="wrong" data-success="right" for="orangeForm-nmr">Number Phone</label>
							<div class="invalid-feedback">Please insert your phone number.</div>
						</div>
						<div class="md-form mb-3">
							<i class="fas fa-envelope prefix"></i>
							<input type="email" name="email" id="orangeForm-email" class="form-control" required>
							<label data-error="wrong" data-success="right" for="orangeForm-email">Email</label>
							<div class="invalid-feedback">Please insert your Email.</div>
						</div>
						<div class="md-form mb-3">
							<i class="fas fa-lock prefix"></i>
							<input type="password" name="password" id="orangeForm-pass1" class="form-control" required>
							<label data-error="wrong" data-success="right" for="orangeForm-pass1">Password</label>
							<div class="invalid-feedback">Please insert your Password.</div>
						</div>
						<div class="md-form mb-3">
							<i class="fas fa-lock prefix"></i>
							<input type="password" name="pass" id="orangeForm-pass2" class="form-control" required>
							<label data-error="wrong" data-success="right" for="orangeForm-pass2">Confirm Password</label>
							<div class="invalid-feedback">Please confirm your Password.</div>
						</div>
						<div class="md-form mb-5 row">
							<div class="file-field">
								<div class="btn purple-gradient btn-sm float-left">
									<span><i class="fas fa-cloud-upload-alt mr-2" aria-hidden="true"></i>Choose files</span>
									<input type="file" name="photo" id="photo" class="custom-file-input" required>
									<div class="invalid-feedback">Please input your image.</div>
								</div>
								<div class="file-path-wrapper">
									<input class="file-path validate" type="text" placeholder="Upload your photo profile">
								</div>
							</div>
						</div>
						<div class="text-center">
							<button type="submit" name="signup" class="btn purple-gradient btn-rounded">Sign Up</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="chgpw" tabindex="-1" role="dialog" aria-labelledby="signupLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header text-center purple-gradient">
					<h3 class="modal-title w-100 font-weight-bold" id="signupLabel">Forgot Password ?</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body mx-3">
					<form action="<?= base_url() ?>home/send" method="post">
						<div class="md-form mb-3">
							<input type="email" name="emailuser" id="orangeForm-pass" class="form-control validate">
							<label data-error="wrong" data-success="right" for="orangeForm-pass" style="margin-top:10px">Input Your Email</label>
						</div>
						<div class="d-flex justify-content-center">
							<button type="submit" name="chgpw" class="btn purple-gradient btn-rounded">Send</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>