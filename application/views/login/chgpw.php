<div>
	<form class="container form-gradient col-md-5 mt-5 pt-5" method="post" action="<?= base_url(); ?>home/ubah">
		<input class="form-control" type="hidden" name="id" value="<?= $login[$no]['id']; ?>">
		<!--Form with header-->
		<div class="card">

			<!--Header-->
			<div class="header pt-3 purple-gradient">

				<div class="container">
					<h3 class="deep-grey-text font-weight-bold text-center mt-3 mb-3 pb-1 mx-5">Change Password</h3>
				</div>

			</div>
			<!--Header-->

			<div class="card-body mx-2">

				<!--Body-->

				<div class="md-form pb-3">
					<i class="fas fa-envelope-open prefix"></i>
					<label>Email</label>
					<input type="email" class="form-control" name="emailuser" value="<?= $login[$no]['Email']; ?>">			
				</div>

				<div class="md-form pb-3">
					<i class="fas fa-low-vision prefix"></i>
					<label>New Password</label>
					<input type="password" name="password" class="form-control">
				</div>

				<div class="md-form pb-3">
					<i class="fas fa-low-vision prefix"></i>
					<label>Confirm Password</label>	
					<input type="password" name="pass" class="form-control">
				</div>

				<!--Grid row-->
				<div class="text-center mt-2 mb-2">
					<button type="submit" name="ubah" class="btn purple-gradient btn-rounded btn-block">Save Changes</button>
				</div>				
			</div>
		</div>
	</form>
</div>