    <!-- Footer -->
    <footer class="page-footer font-small purple-gradient fixed-bottom">

      <!-- Footer Elements -->
      <div class="container">

        <!-- Social buttons -->
        <ul class="list-unstyled list-inline text-center">
          <li class="list-inline-item">
            <a class="btn-floating btn-fb mx-1">
              <i class="fab fa-facebook-f"> </i>
            </a>
          </li>
          <li class="list-inline-item">
            <a class="btn-floating btn-tw mx-1">
              <i class="fab fa-twitter"> </i>
            </a>
          </li>
          <li class="list-inline-item">
            <a class="btn-floating btn-gplus mx-1">
              <i class="fab fa-google-plus-g"> </i>
            </a>
          </li>
          <li class="list-inline-item">
            <a class="btn-floating btn-li mx-1">
              <i class="fab fa-linkedin-in"> </i>
            </a>
          </li>
          <li class="list-inline-item">
            <a class="btn-floating btn-dribbble mx-1">
              <i class="fab fa-dribbble"> </i>
            </a>
          </li>
        </ul>
        <!-- Social buttons -->

      </div>
      <!-- Footer Elements -->

      <!-- Copyright -->
      <div class="footer-copyright text-center black-text py-3">
        <a>Sekian, Terimakasih</a>
      </div>
      <!-- Copyright -->

    </footer>
    <!-- Footer -->

    <!-- SCRIPTS -->
    <script type="text/javascript" src="../assets/mdboot/js/jquery-3.4.0.min.js"></script>
  
    <script type="text/javascript" src="../assets/mdboot/js/bootstrap.bundle.min.js"></script>
  
    <script type="text/javascript" src="../assets/mdboot/js/mdb.js"></script>
    <!-- DataTables -->
    <script type="text/javascript" src="../assets/DataTables/datatables.min.js"></script>
    <script type="text/javascript">
      var table;
      $(document).ready(function() {
        //datatables
        table = $('#table').DataTable({

          "processing": true,
          "serverSide": true,
          // "lengthChange" : false, //Gw hide show entriesnya
          "order": [],

          "ajax": {
            "url": "<?php echo site_url('Tabel/get_data_user') ?>",
            "type": "POST"
          },

          "columnDefs": [{
            "targets": [1, 4, 5],
            "orderable": false,
          }, ],

        });
      });

      $(document).on('click', '.edit', function() {
        var user_id = $(this).attr("id");
        $.ajax({
          url: "<?= base_url() ?>Tabel/edit_user",
          method: "POST",
          data: {
            user_id: user_id
          },
          dataType: "json",
          success: function(data) {
            $('#Nama').val(data.Nama);
            $('#Email').val(data.Email);
            $('#TanggalLahir').val(data.TanggalLahir);
            $('#NomorTelepon').val(data.NomorTelepon);
            $('#Alamat').val(data.Alamat);
            if (data.JenisKelamin == 1) {
              $('#laki').prop('checked', true);
            }
            if (data.JenisKelamin == 2) {
              $('#gadis').prop('checked', true);
            }
            $('.modal-title').text("Edit User");
            $('#user_id').val(user_id);
            $('#isifoto').val(data.image);
          }
        });
      });

      $(document).on('click', '.profil', function() {
        var user_id = $(this).attr("id");
        $.ajax({
          url: "<?= base_url() ?>Tabel/profil_user",
          method: "POST",
          data: {
            user_id: user_id
          },
          dataType: "json",
          success: function(data) {
            $('#NamaProfil').text(data.Nama);
            $('#EmailProfil').text(data.Email);
            $('#TanggalLahirProfil').text(data.TanggalLahir);
            $('#NomorTeleponProfil').text(data.NomorTelepon);
            $('#AlamatProfil').text(data.Alamat);
            if (data.JenisKelamin == 1) {
              $('#JenisKelaminProfil').text("Laki-laki");
            }
            if (data.JenisKelamin == 2) {
              $('#JenisKelaminProfil').text("Perempuan");
            }
            // $('#user_uploaded_image').html(data.image);
          }
        });
      });
    </script>
    </body>

    </html>