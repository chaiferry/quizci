<!DOCTYPE html>
<html>

<head>
    <title><?php echo $judul ?></title>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="../assets/mdboot/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="../assets/mdboot/css/mdb.css" rel="stylesheet">

    <link href="../assets/mdboot/css/all.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="../assets/mdboot/css/style.css" rel="stylesheet">

    <!-- Desain Tabel -->
    <link rel="stylesheet" type="text/css" href="../assets/DataTables/datatables.min.css">
</head>
<body>
    <div id="atas">
        <nav class="navbar navbar-expand-lg navbar-dark purple-gradient" style="margin-bottom:15px">
            <a class="navbar-brand" href="#">
                <img src="../gambar/avengers.png" style="width: 45px; height: 45px;">
            </a>
            <h5><?= $this->session->userdata('Nama'); ?></h5>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse font-weight-normal col-md-10" id="navbarTogglerDemo02">
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item">
                        <a class="nav-link black-text" href="<?php echo base_url() . 'tabel/index'; ?>">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link black-text" href="<?php echo base_url() . 'tabel/data'; ?>">Data Tabel</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link black-text" href="<?php echo base_url() . 'home/logout'; ?>">Log Out</a>
                    </li>
                </ul>
            </div>

        </nav>
    </div>