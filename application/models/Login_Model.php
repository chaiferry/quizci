	<?php

class Login_Model extends CI_Model
{
	var $table = 'tabelemail';
	var $select_column = array('id', 'Nama', 'Email','TanggalLahir','Alamat','NomorTelepon','JenisKelamin', 'image', 'qr_code'); 
	var $column_order = array(null, 'Nama', 'Email', null);
	var $column_search = array('Nama', 'Email');
	var $order = array('id' => 'asc');

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		$this->db->select($this->select_column);
		$this->db->from($this->table);
		$i = 0;

		foreach ($this->column_search as $item) {
			if ($_POST['search']['value']) {
				if ($i === 0) {
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				} else {
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if (isset($_POST['order'])) {
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if ($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->select("*");
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function data_single_user($user_id)
	{
		$this->db->where("id", $user_id);
		$query=$this->db->get('tabelemail');
		return $query->result();
	}

	private function _uploadImage()
	{
	    $config['upload_path']          = './assets/profil/';
	    $config['allowed_types']        = 'gif|jpg|png|jpeg';
	    $config['file_name']            = $this->input->post('photo');
	    $config['overwrite']			= true;
	    $config['max_size']             = 2048; // 2MB

	    $this->load->library('upload', $config);
	    $this->upload->initialize($config);

	    if ($this->upload->do_upload('photo')) {
	        return $this->upload->data("file_name");
	    }
	    
	    return "default.jpg";
	}

	public function getAllLogin()
	{
		return $this->db->get('tabelemail')->result_array();
	}

	public function hapusDataLogin($id)
	{
		$this->db->delete('tabelemail', ['id' => $id]); //Cara nya lebih singkat
	}

	public function SignUp($image_name, $key)
	{
		$data = [
			"Nama" => $this->input->post('nama'),
			"Email" => $this->input->post('email'),
			"Password" => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
			"TanggalLahir" => $this->input->post('tanggallahir'),
			"Alamat" => $this->input->post('alamat'),
			"JenisKelamin" => $this->input->post('jk'),
			"NomorTelepon" => $this->input->post('nomor'),
			"image" => $this->_uploadImage(),
			"qr_code" => $image_name,
			"status" => $key,
		];
		$this->db->insert('tabelemail', $data);
	}

	public function ubahDataLogin($image_name)
	{
		$data = [
			"Nama" => $this->input->post('nama'),
			"Email" => $this->input->post('email'),
			"TanggalLahir" => $this->input->post('tanggallahir'),
			"Alamat" => $this->input->post('alamat'),
			"JenisKelamin" => $this->input->post('jk'),
			"NomorTelepon" => $this->input->post('nomor'),
			"image" => $this->_uploadImage(),
			"qr_code" => $image_name,
		];
		$this->db->where('id', $this->input->post('user_id'));
		$this->db->update('tabelemail', $data);
	}

	public function ubahPassword()
	{
		$data = [
			"Password" => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
		];
		$this->db->where('id', $this->input->post('id'));
		$this->db->update('tabelemail', $data);
	}

	public function verif($id)
	{
		$data = [
			"status" => 'ok',
		];
		$this->db->where('id', $id);
		$this->db->update('tabelemail', $data);
	}
}
