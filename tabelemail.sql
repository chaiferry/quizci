-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 02, 2019 at 01:15 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `loginemail`
--

-- --------------------------------------------------------

--
-- Table structure for table `tabelemail`
--

CREATE TABLE `tabelemail` (
  `id` int(3) NOT NULL,
  `Nama` varchar(25) NOT NULL,
  `Email` varchar(40) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `TanggalLahir` varchar(30) NOT NULL,
  `Alamat` varchar(256) NOT NULL,
  `JenisKelamin` varchar(10) NOT NULL,
  `NomorTelepon` varchar(12) NOT NULL,
  `image` varchar(255) NOT NULL,
  `qr_code` varchar(50) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabelemail`
--

INSERT INTO `tabelemail` (`id`, `Nama`, `Email`, `Password`, `TanggalLahir`, `Alamat`, `JenisKelamin`, `NomorTelepon`, `image`, `qr_code`, `status`) VALUES
(68, 'Ferry Chai', 'Ferryyuno@gmail.com', '$2y$10$qFnqakdM5B1j5qJCzTB4TO7sRkJYsXJZOHo.v5QB/wz/pBYYf7gSy', '2 January, 2000', 'Harapan indah', '1', '082213844350', '321419.jpg', 'Ferry Chai.png', 'ok'),
(69, 'Hesel', 'hesel.natanael07@gmail.com', '$2y$10$R3fBR00/U4UfwyxTET57tOLGUI55mxQZIj4oNzjuuhuKi9HCjjsOq', '6 April, 2000', 'jatiwaringin', '1', '089677235550', '1526022805489.jpg', 'Hesel.png', 'ok'),
(71, 'Ale', 'alenathaniel14@gmail.com', '$2y$10$i8yIYfiYGwVFhyxR0Yz1V.nJMW7Wwqd6tiXKN2nqXOAZ9yjdO0qdO', '14 December, 2000', 'Tanjung Priok', '1', '082297334894', 'momo.jpg', 'Ale.png', 'ok');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tabelemail`
--
ALTER TABLE `tabelemail`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Nama` (`Nama`,`Email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tabelemail`
--
ALTER TABLE `tabelemail`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
